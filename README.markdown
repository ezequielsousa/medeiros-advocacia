# INSTRUÇÕES DE CUSTOMIZAÇÃO DO LAYOUT

- Layout Responsivo
- Fácil de Customizar
- Desenvolvido em HTML e CSS
- Carregamento Super Rápido
- Compartilhe com Facebook
- Resultados para sua empresa em Conversões.

#### INTRODUÇÃO

**Obrigado por escolher nossos projetos!**<br>
Para começar, faça o download do tema para seu desktop e descompacte-o. Após, você deve fazer as mudanças no seu bloco de notas ou utilizar uma IDE de desenvolvimento e programação para fazer alteração no seu tema.

> Customizar temas em html é muito fácil e tentaremos fazer o possível para que você mesmo possa fazer as alterações necessárias.

Se você já possui um plano de hospedagens e uma url de domínio para o seu site, depois que alterar as informações do layout para o nome da sua empresa ou negócio, basta enviar os arquivos para o provedor que gerencia seus dados.

**Porque é legal ter um cartão online?**<br>
Possívelmente porque assim você conhece divulgar seu negócio para qualquer pessoa, em qualquer País que esteja. Talvez, um site possua muitas informações que você não precisa, então optar por um cartão de visita online é a melhor opção no momento.

> Acredite! Um cartão de visitas online trás bastantes beneficios e conversões para sua empresa e você economiza nos gastos com publicidade. Investir em um cartão de visitas online é melhor que investir em cartão físicos, pois, esses cartões fisicos, podem amassar ou serem jogados fora, no lixo.

## COMO ALTERAR AS INFORMAÇÕES

*Para alterar o nome de sua empresa ou marca, titulos, basta abrir o arquivo __index.html__ e buscar pelas tags __h1__ e __span__.*

<br><br>

*Para fazer alterações nas informações de endereços da sua empresa, busque pelas tags __address__ e __ul li__. Essas tags são responsáveis por renderizar as suas informações de contato.*

<br><br>

Para mais informações ou entrar em contato com o desenvolvedor, envie sua mensagem para:<br>
<address>
	Nome: Ezequiel Sousa, Desenvolvedor Web.<br>
	Email: ezequielsousa.contato@outlook.com<br>
</address>

#### SCREENSHOT DESKTOP HEADER
![Screenshot Desktop Header](./img/screenshot-desktop.png)

#### SCREENSHOT DESKTOP CONTACTS
![Screenshot Desktop Contacts](./img/screenshot-desktop-contacts.png)